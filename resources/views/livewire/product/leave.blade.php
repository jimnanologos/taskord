<div class="card mb-4">
    <div class="card-body">
        <button type="button" class="btn w-100 btn-danger text-white fw-bold" wire:click="leaveTeam" wire:loading.attr="disabled">
            <x-heroicon-o-logout class="heroicon" />
            Leave the Team
        </button>
    </div>
</div>
